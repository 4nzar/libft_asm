#include "lib_tests.h"

int		main ( void)
{

	write(1, "Part 1 - Fonctions simples de la libc\n\n", strlen("Part 1 - Fonctions simples de la libc\n\n"));

	test_bzero();
	test_strcat();
	test_isalpha();
	test_isdigit();
	test_isalnum();
	test_isascii();
	test_isprint();
	test_toupper();
	test_tolower();
	puts("");


	write(1, "Part 2 - Fonctions simples mais un peu moins de la libc\n\n", strlen("Part 2 - Fonctions simples mais un peu moins de la libc\n\n"));

	test_strlen();
	test_memset();
	test_memcpy();
	test_strdup();

	return ( 0 );
}