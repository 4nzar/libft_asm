#include "lib_tests.h"

void 	test_isprint_1(void)
{
	int i = 32;
	int ok = 1;

	while (i <= 126)
	{
		if (ft_isprint(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_isprint_2(void)
{
	int i = 31;
	int ok = 1;

	while (i >= -1000)
	{
		if (ft_isprint(i) != 0)
			ok = 0;
		i--;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_isprint_3(void)
{
	int i = 127;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_isprint(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_isprint(void)
{
	write(1, "\tft_isprint : ", 14);
	test_isprint_1();
	test_isprint_2();
	test_isprint_3();
	puts("");
}