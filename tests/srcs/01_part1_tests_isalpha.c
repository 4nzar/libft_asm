#include "lib_tests.h"

static void	test_isalpha_1(void)
{
	int i = 'a';
	int ok = 1;

	while (i <= 'z')
	{
		if (ft_isalpha(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_isalpha_2(void)
{
	int i = 'A';
	int ok = 1;

	while (i <= 'Z')
	{
		if (ft_isalpha(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_isalpha_3(void)
{
	int i = 0;
	int ok = 1;

	while (i < 'A')
	{
		if (ft_isalpha(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_isalpha_4(void)
{
	int i = 123;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_isalpha(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_isalpha_5(void)
{
	int i = 64;
	int ok = 1;

	while (i >= -1000)
	{
		if (ft_isalpha(i) != 0)
			ok = 0;
		i--;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isalpha(void)
{
	write(1, "\tft_isalpha : ", 14);
	test_isalpha_1();
	test_isalpha_2();
	test_isalpha_3();
	test_isalpha_4();
	test_isalpha_5();
	puts("");
}