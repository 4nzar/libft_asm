#include "lib_tests.h"

void 	test_isascii_1(void)
{
	int i = 0;
	int ok = 1;

	while (i <= 127)
	{
		if (ft_isascii(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_isascii_2(void)
{
	int i = -1;
	int ok = 1;

	while (i >= -1000)
	{
		if (ft_isascii(i) != 0)
			ok = 0;
		i--;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_isascii_3(void)
{
	int i = 128;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_isascii(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_isascii(void)
{
	write(1, "\tft_isascii : ", 14);
	test_isascii_1();
	test_isascii_2();
	test_isascii_3();
	puts("");
}
