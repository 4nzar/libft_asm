#include "lib_tests.h"

void 	test_tolower_1(void)
{
	int i = 'A';
	int ok = 1;

	while (i <= 'z')
	{
		if (ft_tolower(i) != tolower(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_tolower_2(void)
{
	int i = -1000;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_tolower(i) != tolower(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_tolower(void)
{
	write(1, "\tft_tolower : ", 14);
	test_tolower_1();
	test_tolower_2();
	puts("");
}
