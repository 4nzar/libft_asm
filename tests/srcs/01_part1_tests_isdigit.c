#include "lib_tests.h"

void	test_isdigit_1(void)
{
	int i = '0';
	int ok = 1;

	while (i <= '9')
	{
		if (ft_isdigit(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isdigit_2(void)
{
	int i = 47;
	int ok = 1;

	while (i >= -1000)
	{
		if (ft_isdigit(i) != 0)
			ok = 0;
		i--;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isdigit_3(void)
{
	int i = 58;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_isdigit(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isdigit(void)
{
	write(1, "\tft_isdigit : ", 14);
	test_isdigit_1();
	test_isdigit_2();
	test_isdigit_3();
	puts("");
}
