#include "lib_tests.h"

int	main(int ac, char **av)
{
	int	fd;
	int	i;

	if (ac == 1)
		ft_cat(0);
	else
	{
		i = 1;
		while (av[i])
		{
			if ((fd = open(av[i], O_RDONLY)) > 0)
			{
				ft_cat(fd);
			}
			else
				perror("open");
			av++;
		}
	}
	return (0);
}