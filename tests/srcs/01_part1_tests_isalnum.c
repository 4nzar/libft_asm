#include "lib_tests.h"

void	test_isalnum_1(void)
{
	int i = 47;
	int ok = 1;

	while (i >= -10000)
	{
		if (ft_isalnum(i) != 0)
			ok = 0;
		i--;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_isalnum_2(void)
{
	int i = '0';
	int ok = 1;

	while (i <= '9')
	{
		if (ft_isalnum(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_isalnum_3(void)
{
	int i = 'A';
	int ok = 1;

	while (i <= 'Z')
	{
		if (ft_isalnum(i) == 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isalnum_4(void)
{
	int i = 123;
	int ok = 1;

	while (i <= 10000)
	{
		if (ft_isalnum(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isalnum_5(void)
{
	int i = 58;
	int ok = 1;

	while (i <= 64)
	{
		if (ft_isalnum(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isalnum_6(void)
{
	int i = 91;
	int ok = 1;

	while (i <= 96)
	{
		if (ft_isalnum(i) != 0)
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isalnum(void)
{
	write(1, "\tft_isalnum : ", 14);
	test_isalnum_1();
	test_isalnum_2();
	test_isalnum_3();
	test_isalnum_4();
	test_isalnum_5();
	test_isalnum_6();
	puts("");
}
