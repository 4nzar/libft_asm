#include "lib_tests.h"

static void	test_strdup_1(void)
{
	if (strcmp(ft_strdup("aaaaa"), "aaaaa") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strdup_2(void)
{
	if (strcmp(ft_strdup(""), "") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strdup_3(void)
{
	if (strcmp(ft_strdup("ok\0null"), "ok\0null") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void	test_strdup(void)
{
	write(1, "\tft_strdup  : ", 13);
	test_strdup_1();
	test_strdup_2();
	test_strdup_3();
	puts("");
}