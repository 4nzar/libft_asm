/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   04_part4_tests_abs.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 17:06:35 by sessaidi          #+#    #+#             */
/*   Updated: 2015/10/14 17:06:40 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_tests.h"

static void	test_abs_1(void)
{
	int i = -100000;
	size_t	j = 100000;
	int ko = 0;

	while (i <= 0)
	{
		if (ft_abs(i) != j)
			ko = 1;
		i++;
		j--;
	}
	if (ko)
		write(1,"KO ", 3);
	else
		write(1,"OK ", 3);	
}

static void	test_abs_2(void)
{
	int i = 0;
	size_t	j = 0;
	int ko = 0;

	while (i <= 100000)
	{
		if (ft_abs(i) != j)
			ko = 1;
		i++;
		j++;
	}
	if (ko)
		write(1,"KO ", 3);
	else
		write(1,"OK ", 3);	
}

void	test_abs(void)
{
	write(1, "\tft_abs     : ", 14);
	test_abs_1();
	test_abs_2();
	puts("");
}

