#include "lib_tests.h"

static void	test_memset_1(void)
{
	char	b1[100], b2[100];

	ft_memset(b1, 42, 100);
	memset(b2, 42, 100);
	if (memcmp(b1, b2, 100) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);

}

static void	test_memset_2(void)
{
	if (memcmp(ft_memset(strdup("abcd"), 'A', 5), memset(strdup("abcd"), 'A', 5), 5) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_memset_3(void)
{
	if (memcmp(ft_memset(strdup("abcd"), 0, 0), memset(strdup("abcd"), 0, 0), 5) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void	test_memset(void)
{
	write(1, "\tft_memset  : ", 13);
	test_memset_1();
	test_memset_2();
	test_memset_3();
	puts("");	
}