#include "lib_tests.h"

void test_strcat_1(void)
{
	char	buf[9];

	bzero(buf, 9);
	ft_strcat(buf, "");
	ft_strcat(buf, "Bon");
	ft_strcat(buf, "j");
	ft_strcat(buf, "our.");
	ft_strcat(buf, "");

	if (strcmp(buf, "Bonjour.") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void	test_strcat_2(void)
{
	char	buf1[20];

	bzero(buf1, 20);
	strcpy(buf1, "bzero");
	ft_strcat(buf1, "bzero");
	if (strcmp(buf1, "bzerobzero") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_strcat(void)
{
	write(1, "\tft_strcat  : ", 14);
	test_strcat_1();
	test_strcat_2();
	puts("");
}