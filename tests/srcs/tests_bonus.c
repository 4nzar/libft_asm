#include "lib_tests.h"

int		main ( void)
{

	write(1, "Part Bonus\n\n", strlen("Part Bonus\n\n"));

	test_atoi();
	test_isspace();
	test_islower();
	test_isupper();
	test_strcmp();
	test_putstr();
	test_puttab();
	test_putnbr();
	test_abs();
	return ( 0 );
}