#include "lib_tests.h"

static void test_puttab_1(void)
{
	write(1, "\n\t1. check if array of string is null : \n", strlen("\n\t1. check if array of string is null : \n"));
	char	**array = 0;
	write(1, "\t\tarray = 0 => ft_puttab : ", strlen("\t\tarray = 0 => ft_puttab : "));
	ft_puttab(array);
}

static void	test_puttab_2(void)
{
	write(1, "\n\t2. check array of strings : \n", strlen("\n\t2. check array of strings : \n"));
	char	**array;
	array = malloc(sizeof(char*) * 4);
	array[0] = "abc";
	array[1] = "def";
	array[2] = "ghi";
	array[3] = "jkl";

	write(1, "\t\tarray = {\"abc\", \"def\", \"ghi\", \"jkl\"} => ft_puttab : ", strlen("\t\tarray = {\"abc\", \"def\", \"ghi\", \"jkl\"} => ft_puttab : "));
	ft_puttab(array);
	free(array);
}

void	test_puttab(void)
{
	write(1, "\tft_puttab  : ", 14);
	test_puttab_1();
	test_puttab_2();
	puts("");
}