#include "lib_tests.h"

static void	test_memcpy_1(void)
{
	char	b1[100], b2[100];

	memset(b1, 33, 100);
	memset(b2, 63, 100);
	ft_memcpy(b1, b2, 100);
	if (memcmp(b1, b2, 100) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_memcpy_2(void)
{
	char	b1[100], b2[100];

	memset(b1, 33, 100);
	memset(b2, 63, 100);
	ft_memcpy(b1, b2, 100);
	if (strcmp(ft_memcpy(b1, b2, 0), b1) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void	test_memcpy(void)
{
	write(1, "\tft_memcpy  : ", 13);
	test_memcpy_1();
	test_memcpy_2();
	puts("");	
}