#include "lib_tests.h"

static void	test_isspace_1(void)
{
	if (ft_isspace(' ') == 1)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_isspace_2(void)
{
	int i = -100000;
	int ok = 1;

	while (i <= 100000)
	{
		if (ft_isspace(i) != isspace(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_isspace(void)
{
	write(1, "\tft_isspace : ", 14);
	test_isspace_1();
	test_isspace_2();
	puts("");
}