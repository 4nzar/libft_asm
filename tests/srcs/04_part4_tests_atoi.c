#include "lib_tests.h"

static void	test_atoi_1(void)
{
	if (ft_atoi("-0") == atoi("-0"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_2(void)
{
	if (ft_atoi("-1") == atoi("-1"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_3(void)
{
	if (ft_atoi("-2147483648") == atoi("-2147483648"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_4(void)
{
	if (ft_atoi("-2147d483648") == atoi("-2147d483648"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_5(void)
{
	if (ft_atoi("-v2147d483648") == atoi("-v2147d483648"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_6(void)
{
	if (ft_atoi("") == atoi(""))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_atoi_7(void)
{
	if (ft_atoi("21474836481") == atoi("21474836481"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_atoi(void)
{
	write(1, "\tft_atoi    : ", 14);
	test_atoi_1();
	test_atoi_2();
	test_atoi_3();
	test_atoi_4();
	test_atoi_5();
	test_atoi_6();
	test_atoi_7();
	puts("");	
}
