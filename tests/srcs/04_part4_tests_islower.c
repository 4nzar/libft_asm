#include "lib_tests.h"

static void	test_islower_1(void)
{
	int i = -10000000;
	int ok = 1;
	while (i <= 1000000)
	{
		if (ft_islower(i) != islower(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_islower(void)
{
	write(1, "\tft_islower : ", 14);
	test_islower_1();
	puts("");
}