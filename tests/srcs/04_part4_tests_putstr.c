#include "lib_tests.h"

static void test_putstr_1(void)
{
	write(1, "\n\t1. check if string is null : \n", strlen("\n\t1. check if string is null : \n"));
	char	*str = 0;
	write(1, "\t\tstr = 0 => ft_putstr : ", strlen("\t\tstr = 0 => ft_putstr : "));
	ft_putstr(str);
}

static void	test_putstr_2(void)
{
	write(1, "\n\t2. check when string length is equal to 0 : \n", strlen("\n\t2. check when string length is equal to 0 : \n"));
	char	*str = "";
	write(1, "\t\tstr = \"\" => ft_putstr : [", strlen("\t\tstr = \"\" => ft_putstr : ["));
	ft_putstr(str);
	write(1, "]\n", 2);
}

static void	test_putstr_3(void)
{
	write(1, "\n\t3. print string : \n", strlen("\n\t3. print string : \n"));
	char	*str = "abcabc\0abc\n";
	write(1, "\t\tstr = \"abcabc\\0abc\\n\" => ft_putstr : [", strlen("\t\tstr = \"abcabc\\0abc\\n\" => ft_putstr : ["));
	ft_putstr(str);
	write(1, "]\n", 2);
}

void	test_putstr(void)
{
	write(1, "\tft_putstr  : ", 14);
	test_putstr_1();
	test_putstr_2();
	test_putstr_3();
	puts("");
}