#include "lib_tests.h"

void	test_bzero_1(void)
{
	char	control[10];
	char	subject[] = "123456789";

	bzero(control, 10);
	ft_bzero(subject, 10);

	if (memcmp(subject, control, 10) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);

	subject[0] = 'a';
	ft_bzero(subject, 0);
	if (strcmp(subject, "a") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_bzero_2(void)
{
	char	teststr[20];

	teststr[0] = 'b';
	teststr[1] = 'z';
	teststr[2] = 'e';
	teststr[3] = 'r';
	teststr[4] = 'o';
	teststr[5] = 'b';
	teststr[6] = 'z';
	teststr[7] = 'e';
	teststr[8] = 'r';
	teststr[9] = 'o';
	teststr[10] = 0;
	teststr[11] = 0;
	teststr[12] = 0;
	teststr[13] = 0;
	teststr[14] = 0;
	teststr[15] = 0;
	teststr[16] = 0;
	teststr[17] = 0;
	teststr[18] = 0;
	teststr[19] = 0;

	char	ref_str[20];

	ref_str[0] = 'b';
	ref_str[1] = 'z';
	ref_str[2] = 'e';
	ref_str[3] = 'r';
	ref_str[4] = 'o';
	ref_str[5] = 'b';
	ref_str[6] = 'z';
	ref_str[7] = 'e';
	ref_str[8] = 'r';
	ref_str[9] = 'o';
	ref_str[10] = 0;
	ref_str[11] = 0;
	ref_str[12] = 0;
	ref_str[13] = 0;
	ref_str[14] = 0;
	ref_str[15] = 0;
	ref_str[16] = 0;
	ref_str[17] = 0;
	ref_str[18] = 0;
	ref_str[19] = 0;

	ft_bzero(teststr + 5, (unsigned int) 4);
	bzero(ref_str + 5, (unsigned int) 4);

	
	if (memcmp(teststr, ref_str, 20) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_bzero_3(void)
{
	char	teststr[20];
	char	ref_str[20];


	teststr[0] = 'b';
	teststr[1] = 'z';
	teststr[2] = 'e';
	teststr[3] = 'r';
	teststr[4] = 'o';
	teststr[5] = 'b';
	teststr[6] = 'z';
	teststr[7] = 'e';
	teststr[8] = 'r';
	teststr[9] = 'o';
	teststr[10] = '0';
	teststr[11] = '0';
	teststr[12] = '0';
	teststr[13] = '0';
	teststr[14] = '0';
	teststr[15] = '0';
	teststr[16] = '0';
	teststr[17] = '0';
	teststr[18] = '0';
	teststr[19] = 0;


	ref_str[0] = 'b';
	ref_str[1] = 'z';
	ref_str[2] = 'e';
	ref_str[3] = 'r';
	ref_str[4] = 'o';
	ref_str[5] = 'b';
	ref_str[6] = 'z';
	ref_str[7] = 'e';
	ref_str[8] = 'r';
	ref_str[9] = 'o';
	ref_str[10] = '0';
	ref_str[11] = '0';
	ref_str[12] = '0';
	ref_str[13] = '0';
	ref_str[14] = '0';
	ref_str[15] = '0';
	ref_str[16] = '0';
	ref_str[17] = '0';
	ref_str[18] = '0';
	ref_str[19] = 0;

	ft_bzero(teststr, 17);
	bzero(ref_str, 17);

	if (memcmp(teststr, ref_str, 20) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void test_bzero_4(void)
{
	char	control1[] = "123456789";
	char	subject1[] = "123456789";

	bzero(control1, 3);
	ft_bzero(subject1, 3);
	if (memcmp(subject1, control1, 10) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}


void	test_bzero(void)
{
	write(1, "\tft_bzero   : ", 14);
	test_bzero_1();
	test_bzero_2();
	test_bzero_3();
	test_bzero_4();
	puts("");
}