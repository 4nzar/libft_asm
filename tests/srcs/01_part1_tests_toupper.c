#include "lib_tests.h"

void 	test_toupper_1(void)
{
	int i = 'A';
	int ok = 1;

	while (i <= 'z')
	{
		if (ft_toupper(i) != toupper(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void 	test_toupper_2(void)
{
	int i = -1000;
	int ok = 1;

	while (i <= 1000)
	{
		if (ft_toupper(i) != toupper(i))
			ok = 0;
		i++;
	}
	if (ok)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);		
}

void	test_toupper(void)
{
	write(1, "\tft_toupper : ", 14);
	test_toupper_1();
	test_toupper_2();
	puts("");
}
