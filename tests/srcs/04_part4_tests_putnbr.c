#include "lib_tests.h"

void test_putnbr(void)
{
	write(1, "\tft_putnbr  : \n", 15);
	write(1, "\t\tft_putnbr(\"-2147483648\") : ", strlen("\t\tft_putnbr(\"-2147483648\") : "));
	ft_putnbr(-2147483648);
	write(1, "\n", 1);
	write(1, "\t\tft_putnbr(\"-2\")          : ", strlen("\t\tft_putnbr(\"-2\")          : "));
	ft_putnbr(-2);
	write(1, "\n", 1);
	write(1, "\t\tft_putnbr(\"2147483647\")  : ", strlen("\t\tft_putnbr(\"2147483647\")  : "));
	ft_putnbr(2147483647);
	write(1, "\n", 1);
	write(1, "\t\tft_putnbr(\"1\")           : ", strlen("\t\tft_putnbr(\"1\")           : "));
	ft_putnbr(1);
	write(1, "\n", 1);
	write(1, "\t\tft_putnbr(\"2017\")        : ", strlen("\t\tft_putnbr(\"2017\")        : "));
	ft_putnbr(2017);
	write(1, "\n", 1);
}