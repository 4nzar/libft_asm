#include "lib_tests.h"

static void	test_strlen_1(void)
{
	if (ft_strlen("") == strlen(""))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strlen_2(void)
{
	if (ft_strlen("abcd\0abcd") == strlen("abcd\0abcd"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strlen_3(void)
{
	if (ft_strlen("123456789123456789123456789123456789123456789") == strlen("123456789123456789123456789123456789123456789"))
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strlen_4(void)
{
	if (ft_strlen(0) == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

void	test_strlen(void)
{
	write(1, "\tft_strlen  : ", 13);
	test_strlen_1();
	test_strlen_2();
	test_strlen_3();
	test_strlen_4();
	puts("");
}