#include "lib_tests.h"

static void	test_strcmp_1(void)
{
	char	buf[9];

	bzero(buf, 9);
	ft_strcat(buf, "");
	ft_strcat(buf, "Bon");
	ft_strcat(buf, "j");
	ft_strcat(buf, "our.");
	ft_strcat(buf, "");

	if (ft_strcmp(buf, "Bonjour.") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strcmp_2(void)
{
	char	buf1[20];

	bzero(buf1, 20);
	strcpy(buf1, "bzero");
	ft_strcat(buf1, "bzero");
	if (ft_strcmp(buf1, "bzerobzero") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);	
}

static void	test_strcmp_3(void)
{
	if (ft_strcmp(ft_strdup("aaaaa"), "aaaaa") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strcmp_4(void)
{
	if (ft_strcmp(ft_strdup(""), "") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

static void	test_strcmp_5(void)
{
	if (ft_strcmp(ft_strdup("ok\0null"), "ok\0null") == 0)
		write(1,"OK ", 3);
	else
		write(1,"KO ", 3);
}

void	test_strcmp(void)
{
	write(1, "\tft_strcmp  : ", 14);
	test_strcmp_1();
	test_strcmp_2();
	test_strcmp_3();
	test_strcmp_4();
	test_strcmp_5();
	puts("");
}