#ifndef	LIB_TESTS_H
# define	LIB_TESTS_H

#include "libfts.h"
#include <stdio.h> 
#include "libfts.h"
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>

void	test_bzero(void);
void	test_strcat(void);
void	test_isalpha(void);
void	test_isdigit(void);
void	test_isalnum(void);
void	test_isascii(void);
void	test_isprint(void);
void 	test_toupper(void);
void	test_tolower(void);
void	test_puts(void);

void	test_strlen(void);
void	test_memset(void);
void	test_memcpy(void);
void	test_strdup(void);

void	test_atoi(void);
void	test_isspace(void);
void	test_islower(void);
void	test_isupper(void);
void	test_strcmp(void);
void	test_putstr(void);
void	test_puttab(void);
void	test_abs(void);
void	test_putnbr(void);
#endif