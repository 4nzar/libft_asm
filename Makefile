# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:11:04 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:11:06 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME            =  libfts.a
SRC_DIR         =  srcs/
INC_DIR         =  includes/
ASM				=  nasm
FLAGS	        =  -f macho64                 
ASM_FILES       =  $(SRC_DIR)ft_bzero.s $(SRC_DIR)ft_isalnum.s
ASM_FILES       += $(SRC_DIR)ft_isalpha.s $(SRC_DIR)ft_isascii.s
ASM_FILES       += $(SRC_DIR)ft_isdigit.s $(SRC_DIR)ft_isprint.s
ASM_FILES       += $(SRC_DIR)ft_puts.s $(SRC_DIR)ft_strcat.s
ASM_FILES       += $(SRC_DIR)ft_strlen.s $(SRC_DIR)ft_tolower.s
ASM_FILES       += $(SRC_DIR)ft_toupper.s $(SRC_DIR)ft_memset.s
ASM_FILES		+= $(SRC_DIR)ft_strdup.s $(SRC_DIR)ft_memcpy.s
ASM_FILES		+= $(SRC_DIR)ft_cat.s $(SRC_DIR)ft_strcmp.s
ASM_FILES		+= $(SRC_DIR)ft_putstr.s $(SRC_DIR)ft_atoi.s
ASM_FILES		+= $(SRC_DIR)ft_putnbr.s $(SRC_DIR)ft_isspace.s
ASM_FILES		+= $(SRC_DIR)ft_isupper.s $(SRC_DIR)ft_islower.s
ASM_FILES		+= $(SRC_DIR)ft_puttab.s $(SRC_DIR)ft_abs.s

ASM_OBJ         =  $(SRC_DIR)ft_bzero.o $(SRC_DIR)ft_isalnum.o
ASM_OBJ			+= $(SRC_DIR)ft_isalpha.o $(SRC_DIR)ft_isascii.o
ASM_OBJ         += $(SRC_DIR)ft_isdigit.o $(SRC_DIR)ft_isprint.o
ASM_OBJ			+= $(SRC_DIR)ft_puts.o $(SRC_DIR)ft_strcat.o
ASM_OBJ         += $(SRC_DIR)ft_strlen.o $(SRC_DIR)ft_tolower.o
ASM_OBJ			+= $(SRC_DIR)ft_toupper.o $(SRC_DIR)ft_memset.o
ASM_OBJ			+= $(SRC_DIR)ft_strdup.o $(SRC_DIR)ft_memcpy.o
ASM_OBJ			+= $(SRC_DIR)ft_cat.o $(SRC_DIR)ft_strcmp.o
ASM_OBJ			+= $(SRC_DIR)ft_putstr.o $(SRC_DIR)ft_atoi.o
ASM_OBJ			+= $(SRC_DIR)ft_putnbr.o $(SRC_DIR)ft_isspace.o
ASM_OBJ			+= $(SRC_DIR)ft_isupper.o $(SRC_DIR)ft_islower.o
ASM_OBJ			+= $(SRC_DIR)ft_puttab.o $(SRC_DIR)ft_abs.o

all     :       $(NAME)

$(NAME) :
	@nasm $(FLAGS) $(SRC_DIR)ft_bzero.s
	@echo "ft_bzero"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isalnum.s
	@echo "ft_isalnum"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isalpha.s
	@echo "ft_isalpha"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isascii.s
	@echo "ft_isascii" 
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isdigit.s
	@echo "ft_isdigit"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isprint.s
	@echo "ft_isprint"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_puts.s
	@echo "ft_puts"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_strcat.s
	@echo "ft_strcat"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_strlen.s
	@echo "ft_strlen"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_tolower.s
	@echo "ft_tolower"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_toupper.s
	@echo "ft_toupper"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_memset.s
	@echo "ft_memset"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_memcpy.s
	@echo "ft_memcpy"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_strdup.s
	@echo "ft_strdup"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_cat.s
	@echo "ft_cat"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_strcmp.s
	@echo "ft_strcmp"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_putstr.s
	@echo "ft_putstr"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_atoi.s
	@echo "ft_atoi"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_putnbr.s
	@echo "ft_putnbr"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isspace.s
	@echo "ft_isspace"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_islower.s
	@echo "ft_islower"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_isupper.s
	@echo "ft_isupper"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_puttab.s
	@echo "ft_puttab"
	@$(ASM) $(FLAGS) $(SRC_DIR)ft_abs.s
	@echo "ft_abs"
	@ar -rcs $(NAME) $(ASM_OBJ)
	ranlib $(NAME)

run_tests:
	@make re
	@make -C tests/
	@cd tests/

clean:
	@cd $(SRC_DIR)
	@rm -rf $(ASM_OBJ)
	@cd ..


fclean: clean
	@rm -rf $(NAME)

re: fclean all
