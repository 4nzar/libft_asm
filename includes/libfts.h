/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 16:30:54 by sessaidi          #+#    #+#             */
/*   Updated: 2015/07/22 17:11:24 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTS_H
# define LIBFTS_H

#include    <stdlib.h>
#include	<unistd.h>
#include	<string.h>

void	ft_bzero(void *s, size_t n);

int	    ft_isalpha(int c);
int	    ft_isdigit(int c);
int	    ft_isalnum(int c);
int	    ft_isascii(int c);
int	    ft_isprint(int c);
int     ft_isupper(int c);
int     ft_islower(int c);
int	    ft_toupper(int c);
int	    ft_tolower(int c);
int 	ft_isspace(int c);

void	*ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);

char	*ft_strdup(const char *s1);
char	*ft_strcat(char *s1, const char *s2);
size_t	ft_strlen(char *s);
char	*ft_strjoin(char const *s1, char const *s2);
int	    ft_strcmp(const char *s1, const char *s2);
char	*ft_strnew(size_t size);

void    ft_putnbr(int n);
int     ft_putstr(const char *s);
void	ft_puttab(char **tab);
int	    ft_puts(const char *s);
int 	ft_atoi(const char *str);
void	ft_cat(int fd);
size_t 	ft_abs(int n);

#endif
