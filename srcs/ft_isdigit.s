# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isdigit.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:12:07 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:12:12 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISDIGIT						                               ;
; PROTOTYPE     int	isdigit(int c)				                          	   ;
; DESIGNATION   the ft_isidigit() function check if the passed character is a  ;
;				decimal digit character.							  		   ;
; RETURN	    the ft_isdigit() function returns non-zero value if c is a     ;
;				digit, else it returns 0									   ;
; ============================================================================ ;

section	.text
	global	_ft_isdigit

	_ft_isdigit	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		cmp		rdi, 0x30	; compare rdi to '0'
		jl		_false		; if rdi is smaller to '0' go to _false
		cmp		rdi, 0x39	; compare rdi to '9'
		jle		_true		; if rdi is smaller or equal to '9' go to _true
		jmp		_false		; go to _false

	_false		:
		xor		rax, rax	; initialize rax to 0
		jmp		_return		; got to _return
	_true		:
		mov		rax, 1		; initialize rax to 1
		jmp		_return		; go to _return
	
	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
