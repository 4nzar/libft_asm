# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strdup.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:14:14 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:14:36 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_STRDUP						                               ;
; PROTOTYPE     char	*ft_strdup(const char *s1)                      	   ;
; DESIGNATION   the ft_strdup() function return a pointer to a new string,     ;
;               which is a duplicate of the string pointed to by s1. The       ;
;               returned pointer can be passed to free(). A null pointer is    ;
;               returned if the new string cannot be created                   ;
; RETURN	    the ft_strdup() function return a pointer to a new string on   ;
;               success. Otherwise, it shall return a null pointer             ;
; ============================================================================ ;

extern  _ft_strlen
extern  _ft_memcpy
extern  _malloc

section .text
    global  _ft_strdup

    _ft_strdup   :
        push    rbp             ; save rpb state
        mov     rbp, rsp        ; set the base pointer as it was the top of the stack
        test    rdi, rdi        ; test if rdi exist
		jz      _return         ; if doesnt exist go to _return
        push    rdi
        call    _ft_strlen      ; call ft_strlen function
        push    rax             ; save result to the stack
        inc     rax             ; increment rax for '/0' 
        
        mov     rdi, rax        ; set 1st argument of malloc to rax
        call    _malloc         ; call malloc
        
        test    rax, rax        ; test if rax (result of malloc) is not null
        jz      _return         ; if null go to _return
        
        mov     rdi, rax        ; set rdi with result of malloc (rax) 
        pop     rdx             ; get result of ft_strlen for rdi from the stack
        pop     rsi             ; get argument of ft_strdup from the stack to rsi
        call    _ft_memcpy      ; call ft_memcpy function

    _return     :
        leave                   ; releases the stack frame 
		ret					    ; return rax
