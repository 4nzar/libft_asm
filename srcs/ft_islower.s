# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_islower.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:21:52 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:21:54 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISLOWER						                               ;
; PROTOTYPE     int	ft_islower(int c);                          	           ;
; DESIGNATION   the ft_islower() function tests for any character if the       ;
;               character is an lowercase letter                               ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     141 ``a''    142 ``b''     143 ``c''     144 ``d''     145 ``e''         ;
;     146 ``f''    147 ``g''     150 ``h''     151 ``i''     152 ``j''         ;
;     153 ``k''    154 ``l''     155 ``m''     156 ``n''     157 ``o''         ;
;     160 ``p''    161 ``q''     162 ``r''     163 ``s''     164 ``t''         ;
;     165 ``u''	   166 ``v''     167 ``w''     170 ``x''     171 ``y''         ;
;     172 ``z''                                                   ;
; RETURN	    the ft_islower() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

section	.text
	global	_ft_islower

	_ft_islower	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		cmp		rdi, 0x61	; compare if rdi is equal to 'a' (hexa value)
		jl		_false		; if rdi is smaller than 'a' go to _false
		cmp		rdi, 0x7a	; else compare if rdi is equal to 'z' (hexa value)
		jle		_true		; if rdi is smaller or equal to 'z' go to _true
		jmp		_false		; else go to _false

	_false		:
		xor		rax, rax	; initialize rax to 0
		jmp		_return		; got to _return
	_true		:
		mov		rax, 1		; initialize rax to 1
		jmp		_return		; go to _return
	
	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
