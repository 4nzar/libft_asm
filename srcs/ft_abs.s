# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_abs.s                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:21:18 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:21:20 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ABS                                                         ;
; PROTOTYPE     size_t 	ft_abs(int n);                                         ;
; DESIGNATION   the ft_abs() function check if the value in parameter is       ;
;               negative, if the value is negative then return the absolute    ;
;               value.                                                         ;
; RETURN        the ft_abs() function return an unsigned int                   ;
; ============================================================================ ;

section .text
    global _ft_abs

	_ft_abs		:
		push	rbp				;
		mov		rbp, rsp		; save stack frame
		movsx 	rax, edi		; set rax with rdi value
		cmp 	rax, 0			; compare if rax is equal to 0
		jl		_neg			; if smaller then go to _neg
		leave					; release stack frame
		ret						; return result

	_neg		:
		neg 	rax				; get absolute value of rax
		leave					; then release stack frame
		ret						; return result
