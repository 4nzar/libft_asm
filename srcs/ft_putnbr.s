# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_putnbr.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:22:52 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:22:54 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_PUTNBR                                                      ;
; PROTOTYPE     void    ft_putnbr(int n);                                      ;
; DESIGNATION   the ft_putnbr() function writes the integer n on the screen.   ;
; RETURN        the ft_putnbr() function return t nothing.	                   ;
; ============================================================================ ;

%ifidn __OUTPUT_FORMAT__, macho64 
	%define SYS_WRITE 0x2000004
%elifidn __OUTPUT_FORMAT__, elf64
	%define SYS_WRITE 1
%endif

section .rodata
	MINUS :	db	48, 45, 0

section	.text
	global	_ft_putnbr

	_ft_putnbr	:
	    push    rbp                         ;
	    mov     rbp, rsp                    ; save stack frame
	    movsx   rbx, edi                    ; set rbx
	    cmp     rbx, 0x0                    ; compare if rbx is negative
	    jge     _printnb                    ; if not then go to _printnb
	    neg     rbx                         ; get the absolute value of rbx
	    mov     rax, SYS_WRITE              ; set rax with write syscall value 
	    mov     rdi, 1                      ; set rdi t 1 for stdout
	    lea		rsi, [rel MINUS + 1]        ; set rsi with "-"
	    mov     rdx, 1                      ; set rdx to 1 for length
	    syscall                             ; call write syscall

	_printnb        :
	    cmp     rbx, 0x9                    ; compare if rbx is equal to 9
	    jg      _recursion                  ; if greater then go to _recursion
	    lea		rsi, [rel MINUS]            ; set rsi with "0"
	    add     [rsi], rbx                  ; then add rbx, in order to display a digit
	    mov     rdi, 1                      ; set rdi to 1 for stdout
	    mov     rax, SYS_WRITE              ; set rax with write syscall value
	    mov     rdx, 1                      ; set rdx to 1 (display one character)
	    push	rsi 						; save rsi on the stack
	    syscall                             ; call write syscall
		pop		rsi 						; set rsi with value on the stack
		mov		byte [rsi], 48				; set byte of rsi to 48 ('0' char)
		jmp     _return                     ; then go to _return (exit)

	_recursion  :
	    xor     rdx, rdx                    ; initialize rdx to 0
	    xor     rax, rax                    ; initialize rax to 0
	    mov     rcx, 10                     ; set rcx to 10
	    mov     rax, rbx                    ; set rax with rbx value
	    div     rcx                         ; div rax by rcx (10) 
	    push    rdx                         ; rdx = remainder save it in the stack
	    mov     rdi, rax                    ; set rdi with rax
	    call    _ft_putnbr                  ; call _putnbr
	    pop     rdi                         ; get the remainder from the stack and set rdi with it
	    call    _ft_putnbr                  ; call _putnbr
	    jmp     _return                     ; if finished go to _return

	_return     :
	    leave                               ; releases the stack frame 
	    ret                                 ; return rax

