# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_toupper.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:12:43 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:12:45 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_TOUPPER						                               ;
; PROTOTYPE     int	ft_toupper(int c);                          	           ;
; DESIGNATION   the ft_toupper() function converts a lower-case letter to the  ;
;               corresponding upper-case letter.                               ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     141 ``a''    142 ``b''     143 ``c''     144 ``d''     145 ``e''         ;
;     146 ``f''    147 ``g''     150 ``h''     151 ``i''     152 ``j''         ;
;     153 ``k''    154 ``l''     155 ``m''     156 ``n''     157 ``o''         ;
;     160 ``p''    161 ``q''     162 ``r''     163 ``s''     164 ``t''         ;
;     165 ``u''	   166 ``v''     167 ``w''     170 ``x''     171 ``y''         ;
;     172 ``z''                                                                ;
; RETURN	    if the argument is a lower-case letter, the ft_toupper()       ;
;               function returns the corresponding upper-case letter, otherwise;
;               The arguments is returned unchanged.                           ;
; ============================================================================ ;

section	.text
	global	_ft_toupper

	_ft_toupper	: 
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		mov		rax, rdi	; set pointer of rdi to rax
		cmp		rax, 0x61	; compare if rax is equal to 'a' (hex value)
		jl		_return		; if smaller then go to _return
		cmp		rax, 0x7a   ; else compare if rax is equal to 'z' (hex value)
		jg		_return		; if greater then go to _return
		sub		rax, 32	    ; else substract 32 to rax (view ascii table)

	_return		:
        leave           	; releases the stack frame 
		ret					; return rax
