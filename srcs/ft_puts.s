# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_puts.s                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:13:46 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:13:51 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_PUTS                                                        ;
; PROTOTYPE     int	ft_puts(const char *s);                                	   ;
; DESIGNATION   the ft_puts() function writes thes string s, and a terminating ;
;               new line character to the stream output                        ;
; RETURN	    the ft_() function return the length of the string on          ;
;               success else zero											   ;
; ============================================================================ ;

%ifidn __OUTPUT_FORMAT__, macho64 
	%define SYS_WRITE 0x2000004
%elifidn __OUTPUT_FORMAT__, elf64
	%define SYS_WRITE 1
%endif

extern	_ft_strlen

section .data
    NIL        : db  '(null)', 10  ; error message if string is null

section	.text

    global  _ft_puts

    _ft_puts :
        push    rbp             	; save rpb state
        mov     rbp, rsp        	; set the base pointer as it was the top of the stack
        test    rdi, rdi        	; test if rdi is NULL
        jz      _null           	; if rdi is null go to _null
        xor		rcx, rcx			; initialize rcx to 0
		push    rdi					; save rdi state on the stack
		call	_ft_strlen			; call _ft_strlen
		cmp		rax, 0				; compare if result of _ft_strlen (rax) is equal to 0
		je		_new_line			; if equal to 0 then go to _new_line
		mov		rcx, rax			; save result of _ft_strlen (rax) in rcx
		pop		rsi					; get the last state of rdi and set rsi with it
		mov		rdi, 1				; set rdi to 1 (stdout)
		mov		rdx, rax			; set rdx with the length of the string 
		mov		rax, SYS_WRITE		; set rax with syscall write value
		syscall						; call syscall

	_new_line	:
		mov		rax, SYS_WRITE		; set rax with syscall write value
		mov		rdi, 1				; set rdi to 1 (stdout)
		lea		rsi, [rel NIL + 6]	; set rsi with the value of NIL + 6 (newline char)
		mov		rdx, 1				; set rdx to 1
		syscall						; call syscall 
		add   rax, rcx				; add to rax the last result of _ft_strlen
        leave               		; releases the stack frame 
		ret							; return rax
    
    _null   :
        mov     rax, SYS_WRITE  	; write syscall
        mov     rdi,1          	    ; set rdi to 2 (stderr : 2)
        lea     rsi, [rel NIL] 	    ; set the pointer of NULL to rsi
        mov     rdx, 7          	; set rdx to 6 (length message NULL)
        syscall                 	; call syscall (write)
		leave                   	; releases the stack frame 
        ret                     	; return rax
