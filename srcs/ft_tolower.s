# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_tolower.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:12:35 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:12:37 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_TOLOWER						                               ;
; PROTOTYPE     int	ft_tolower(int c);                          	           ;
; DESIGNATION   the ft_tolower() function converts a upper-case letter to the  ;
;               corresponding lower-case letter.                               ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     101 ``A''    102 ``B''     103 ``C''     104 ``D''     105 ``E''         ;
;     106 ``F''    107 ``G''     110 ``H''	   111 ``I''	 112 ``J''         ;
;     113 ``K''    114 ``L''     115 ``M''	   116 ``N''	 117 ``O''         ;
;     120 ``P''    121 ``Q''     122 ``R''     123 ``S''	 124 ``T''         ;
;     125 ``U''    126 ``V''     127 ``W''	   130 ``X''	 131 ``Y''         ;
;     132 ``Z''                                                                ;
; RETURN	    if the argument is a upper-case letter, the ft_tolower()       ;
;               function returns the corresponding lower-case letter, otherwise;
;               The arguments is returned unchanged.                           ;
; ============================================================================ ;

section	.text
	global	_ft_tolower

	_ft_tolower	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		mov		rax, rdi	; set pointer of rdi to rax			
		cmp		rax, 0x41	; compare if rax is equal to 'A' (hex value)
		jl		_return		; if smaller then go to _return
		cmp		rax, 0x5a	; else compare if rax is equal to 'Z' (hex value)
		jg		_return		; if greater then go to _return
		add		rax, 32		; else add 32 to rax (view ascii table)

	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
