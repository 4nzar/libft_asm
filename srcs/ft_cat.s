# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_cat.s                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:21:07 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:21:08 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_CAT    						                               ;
; PROTOTYPE     void	ft_cat(int fd);                                  	   ;
; DESIGNATION   the ft_cat() function reads files sequentially, writing them   ;
;               to the standard output.                                        ;
; RETURN	    the ft_cat() function return nothing                           ;
; ============================================================================ ;

%ifidn __OUTPUT_FORMAT__, macho64   ; if compile with macho64 format
    %define SYS_READ    0x2000003   ; define sys_read to 0x2000003
    %define SYS_WRITE   0x2000004   ; define sys_write to 0x2000004
%elifidn __OUTPUT_FORMAT__, elf64   ; if compile with elf64 format
    %define SYS_READ    0           ; define sys_read to 0
    %define SYS_WRITE   1           ; define sys_write to 1
%endif

%define	BUFSIZE	2048                ; we can define a bufsize

section .bss
    buffer  : resb BUFSIZE          ; declare buffer variable
    .size   : equ $ - buffer;       ; declare .size variable set to size of buffer
    
section .text
    global  _ft_cat
    
    _ft_cat          :
        push    rbp                 ; save rpb state
        mov     rbp, rsp            ; set the base pointer as it was the top of the stack
        push    rdi                 ; save rdi state (pointer)
    
    _read_fd        :
        pop     rdi                 ; get rdi state from the stack (pointer)
        
        mov     rax, SYS_READ       ; read syscall 
        lea     rsi, [rel buffer]   ; get pointer of buffer on rsi
        mov     rdx, buffer.size    ; set rdx to buffer.size
        syscall                     ; call syscall (read)
        
        push    rdi                 ; save rdi state (pointer)
        
        cmp     rax, 0x0            ; compare if rax equal to 0 (result of read) 
        jle      _exit              ; if smaller or equal to 0 go to _exit
        mov     rdx, rax
        mov     rax, SYS_WRITE      ; write syscall
        mov     rdi, 1              ; set rdi (1st argument) to 1 (stdout)
        lea     rsi, [rel buffer]   ; set pointer of buffer on rsi 
        syscall                     ; call syscall (write)
        
        jmp     _read_fd            ; go to _read_fd (until rax <= 0)
        
    _exit     :
        pop     rdi                 ; get rdi state from the stack (pointer)
        xor     rax, rax            ; initialiaze rax to 0
        leave                       ; releases the stack frame 
        ret                         ; return result
