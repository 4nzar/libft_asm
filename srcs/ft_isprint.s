# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isprint.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:12:19 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:12:20 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISPRINT						                               ;
; PROTOTYPE     int	ft_isprint(int c);                          	           ;
; DESIGNATION   the ft_isprint() function tests for any printing character,    ;
;	        	including space (' ').                                         ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     040 sp	   041 ``!''	 042 ``"''     043 ``#''     044 ``$''         ;
;     045 ``%''	   046 ``&''	 047 ``'''     050 ``(''     051 ``)''         ;
;     052 ``*''	   053 ``+''	 054 ``,''     055 ``-''     056 ``.''         ;
;     057 ``/''	   060 ``0''	 061 ``1''     062 ``2''     063 ``3''         ;
;     064 ``4''	   065 ``5''	 066 ``6''     067 ``7''     070 ``8''         ;
;     071 ``9''	   072 ``:''	 073 ``;''     074 ``<''     075 ``=''         ;
;     076 ``>''	   077 ``?''	 100 ``@''     101 ``A''     102 ``B''         ;
;     103 ``C''	   104 ``D''	 105 ``E''     106 ``F''     107 ``G''         ;
;     110 ``H''	   111 ``I''	 112 ``J''     113 ``K''     114 ``L''         ;
;     115 ``M''	   116 ``N''	 117 ``O''     120 ``P''     121 ``Q''         ;
;     122 ``R''	   123 ``S''	 124 ``T''     125 ``U''     126 ``V''         ;
;     127 ``W''	   130 ``X''	 131 ``Y''     132 ``Z''     133 ``[''         ;
;     134 ``\''	   135 ``]''	 136 ``^''     137 ``_''     140 ```''         ;
;     141 ``a''	   142 ``b''	 143 ``c''     144 ``d''     145 ``e''         ;
;     146 ``f''	   147 ``g''	 150 ``h''     151 ``i''     152 ``j''         ;
;     153 ``k''	   154 ``l''	 155 ``m''     156 ``n''     157 ``o''         ;
;     160 ``p''	   161 ``q''	 162 ``r''     163 ``s''     164 ``t''         ;
;     165 ``u''	   166 ``v''	 167 ``w''     170 ``x''     171 ``y''         ;
;     172 ``z''	   173 ``{''	 174 ``|''     175 ``}''     176 ``~''         ;
; RETURN	    the ft_isprint() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

section	.text
	global	_ft_isprint

	_ft_isprint	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		cmp		rdi, 0x20	; compare if rdi is equal to space (hexa value)
		jl		_false		; if rdi is smaller than 0x20 go to false
		cmp		rdi, 0x7e	; else compare if rdi is equal to '~' (hexa value)
		jle		_true		; if rdi is smaller or equal to 0x7e go to true
		jmp		_false		; else go to false

	_false		:
		xor		rax, rax	; initialize rax to 0
		jmp		_return		; got to _return
	_true		:
		mov		rax, 1	    ; initialize rax to 1
		jmp		_return		; go to _return
	
	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
