# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_puttab.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:22:39 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:22:40 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_PUTTAB						                               ;
; PROTOTYPE     void	ft_puttab(char **tab);                           	   ;
; DESIGNATION   the ft_puttab() function writes on the stdout values of an     ;
;               array of string, with a newline between every values           ;
; RETURN	    the ft_puttab() function return nothing                        ;
; ============================================================================ ;

extern	_ft_puts

section .text
	global	_ft_puttab

	_ft_puttab:
		push	rbp					;
		mov		rbp, rsp			; save stackframe
		test	rdi, rdi			; check if rdi is null
		jz		_null				; if null then go to _return
		mov		r8, rdi				; set r8 with the first pointer of rdi

	_loop		:
		mov		rdi, [r8] 			; set rdi with the value of rdi at position r8
		cmp		rdi, 0				; compare if rdi at position r8 is equal to 0
		je		_return				; if equal then go to _return
		call 	_ft_puts			; call ft_puts (for printing value of )
		add 	r8, 8				; add 8 to r8 to go tothe next value
		jmp		_loop				; then repeat

	_return		:
        leave               		; releases the stack frame 
		ret							; return rax

    _null   :
    	call	_ft_puts			; call ft_puts (print null in stderr)
        leave                   	; releases the stack frame 
        ret                     	; return rax
