# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strcat.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:14:07 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:14:08 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_STRCAT						                               ;
; PROTOTYPE     char    *ft_strcat(char *s1, const char *s2);              	   ;
; DESIGNATION   the ft_strcat() function writes len zero bytes to the string b.;
;               If len is zero, ft_bzero() does nothing.                       ;
; RETURN	    the ft_strcat() function return nothing                        ;
; ============================================================================ ;

section .text
    global  _ft_strcat

    _ft_strcat   :
        push    rbp                     ; save rpb state
        mov     rbp, rsp                ; set the base pointer as it was the top of the stack
        sub     rbp, 0                  ; no local variables, most compilers will omit this line
        push    rdi                     ; save pointer rdi
        test    rdi, rdi                ; test if rdi exist
		jz      _return                 ; if does not exist go to _return
        xor     al, al                  ; initialize al
        mov     r9, 0                   ; set r9 to 0
        mov     r10, 0                  ; set r10 to 0

    _loop       :
        cmp     byte [rdi + r9], 0x0    ; compare byte of rdi is equal to null
        jle     _concat                 ; if equal go to _concat
        inc     r9                      ; increment rdi
        jmp     _loop                   ; (repeat) go to _loop
    
    _concat     :
        mov     al, byte [rsi + r10]    ; copy [rsi] in al
        test    al, al                  ; test if al is not null
        jz      _return                 ; if null then go to _return
        mov     byte [rdi + r9], al     ; copy al to byte [rdi]
        inc     r9                      ; increment rdi
        inc     r10                     ; increment rsi
        jmp     _concat                 ; (repeat) go to _concat
    
    _return     :
        mov     byte [rdi + r9], 0x0    ; add '/0' to rax
        pop     rax                     ; get first pointer of rdi
        leave                           ; releases the stack frame 
		ret					            ; return rax
