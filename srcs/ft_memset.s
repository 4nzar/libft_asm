# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_memset.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:13:36 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:13:39 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_MEMSET						                               ;
; PROTOTYPE     void	*ft_memset(void *b, int c, size_t len);          	   ;
; DESIGNATION   the ft_memset() function writes len bytes of value c           ;
;               (converted to an unsigned char) to the string b.               ;
; RETURN	    the ft_memset() function returns its first argument.           ;
; ============================================================================ ;

section .text
    global  _ft_memset

    _ft_memset        :
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
        push    rdi         ; save pointer rdi on the stack
        mov     rax, rsi    ; set rax to rsi value
        mov     rcx, rdx    ; set counter rcx to rdx
        cld                 ; set DF to 0
        rep     stosb       ; repeat copy rax register to rdi pointer
        pop     rax         ; get pointer from stack
        leave               ; releases the stack frame 
		ret					; return rax
