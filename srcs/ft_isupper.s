# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isupper.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:21:59 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:22:00 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISUPPER						                               ;
; PROTOTYPE     int	ft_isupper(int c);                          	           ;
; DESIGNATION   the ft_isupper() function tests for any character if the       ;
;               character is an uppercase letter                               ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     101 ``A''    102 ``B''     103 ``C''     104 ``D''     105 ``E''         ;
;     106 ``F''    107 ``G''     110 ``H''	   111 ``I''	 112 ``J''         ;
;     113 ``K''    114 ``L''     115 ``M''	   116 ``N''	 117 ``O''         ;
;     120 ``P''    121 ``Q''     122 ``R''     123 ``S''	 124 ``T''         ;
;     125 ``U''    126 ``V''     127 ``W''	   130 ``X''	 131 ``Y''         ;
;     132 ``Z''                                                                ;
; RETURN	    the ft_isupper() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

section	.text
	global	_ft_isupper

	_ft_isupper	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		cmp		rdi, 0x41	; compare if rdi is equal to 'A' (hexa value)
		jl		_false		; if rdi is smaller than 'A' go to _false
		cmp		rdi, 0x5a	; else compare if rdi is equal to 'Z' (hexa value)
		jle		_true		; if rdi is smaller or equal to 'Z' go to _true
		jmp		_false		; else go to _false

	_false		:
		xor		rax, rax	; initialize rax to 0
		jmp		_return		; got to _return
	_true		:
		mov		rax, 1		; initialize rax to 1
		jmp		_return		; go to _return
	
	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
