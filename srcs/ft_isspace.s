# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isspace.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:22:24 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:22:26 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISUPPER						                               ;
; PROTOTYPE     int	ft_isspace(int c);                          	           ;
; DESIGNATION   the ft_isspace() function tests for the white-space characters ;
;																			   ;
;            ``\t''   ``\n''    ``\v''    ``\f''    ``\r''    `` ''            ;
; RETURN	    the ft_isupper() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

section	.text
	global	_ft_isspace

	_ft_isspace	:
		push	rbp			;
		mov		rbp, rsp	; save stackframe
		xor		rax, rax	; initialize rax to 0
		cmp		rdi, 0x8	; compare if rdi is equal to backspace (hex value)
		jle		_return		; return 0 if smaller or equal to backspace
		cmp		rdi, 0xe	; compare if rdi is equal to shift out (hex value)
		jl		_true		; return true if strictly smaller than shift out
		cmp		rdi, 0x20	; else compare if rdi is equal to space (hex value)
		je		_true		; then return true else go by default to _return

	_return		:
        leave               ; releases the stack frame 
        ret                 ; return result

	_true		:
		mov		rax, 1		; set rax to 1
		jmp		_return		; then go to _return
