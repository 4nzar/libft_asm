# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isascii.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:11:52 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:11:54 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISASCII						                               ;
; PROTOTYPE     int	ft_isascii(int c);                          	           ;
; DESIGNATION   the ft_isascii() function tests for any printing character,    ;
;	        	including space (' ').                                         ;
;                                                                              ;
; RETURN	    the ft_isprint() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

section	.text
	global	_ft_isascii

	_ft_isascii	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		cmp		rdi, 0x0	; compare if rdi is equal to NULL (1st ascii value hex)
		jl		_false		; if smaller than NULL go to false
		cmp		rdi, 0x7f	; else compare if rdi is equal to DEL (last ascii value)
		jle		_true		; if smaller or equal to DEL go to _true
		jmp		_false		; else go to _false

	_false		:
		xor		rax, rax	; initialize rax to 0
		jmp		_return		; got to _return
		
	_true		:
		mov		rax, 1		; initialize rax to 1
		jmp		_return		; go to _return
	
	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
