# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_memcpy.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:12:53 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:12:54 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_MEMCPY						                               ;
; PROTOTYPE     void	*ft_memcpy(void *dst, const void *src, size_t n);  	   ;
; DESIGNATION   the ft_memcpy() function writes len bytes of value c           ;
;               (converted to an unsigned char) to the string b.               ;
; RETURN	    the ft_memcpy() function returns its first argument.           ;
; ============================================================================ ;

section .text
    global  _ft_memcpy

    _ft_memcpy        :
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        push    rdi         ; save pointer rsi on the stack
        mov     rcx, rdx    ; set counter rcx to rdx
        cld                 ; set DF to 0
        rep     movsb       ; repeat
        pop     rax         ; get pointer from stack
        leave               ; releases the stack frame 
		ret					; return rax
