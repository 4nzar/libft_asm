# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_bzero.s                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:11:20 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:11:24 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_BZERO						                               ;
; PROTOTYPE     void	ft_bzero(void *s, size_t n);                      	   ;
; DESIGNATION   the ft_bzero() function writes len zero bytes to the string b. ;
;               If len is zero, ft_bzero() does nothing.                       ;
; RETURN	    the ft_bzero() function return nothing                         ;
; ============================================================================ ;

section .text
    global  _ft_bzero

    _ft_bzero        :
        push    rbp             ; save rpb state
        mov     rbp, rsp        ; set the base pointer as it was the top of the stack
        sub     rsp, 0          ; no local variables, most compilers will omit this line
        push    rdi             ; save rdi state (pointer)
        
    _loop           :
        cmp     rsi, 0          ; compare if rsi is equal to 0
        jle     _return         ; if rsi is equal to 0 go to _return
        mov     byte [rdi], 0   ; else set byte of rdi to 0
        inc     rdi             ; increment rdi to 1
        dec     rsi             ; decrement rsi to 1
        jmp     _loop           ; (repeat) go to ft_bzero

    _return        :
        pop     rdi             ; get rdi state to the stack (pointer)
        mov     rax, rdi
        leave                   ; releases the stack frame 
        ret                     ; return result
