# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strlen.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:14:45 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:14:48 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_STRLEN						                               ;
; PROTOTYPE     size_t	ft_strlen(const char *s);                          	   ;
; DESIGNATION   the ft_strlen() function calculates the length of the string s,;
;	        	excluding the terminating null byte                    		   ;
; RETURN	    the ft_strlen() function return the length of the string       ;
; ============================================================================ ;

section	.text
	global	_ft_strlen
	
	_ft_strlen	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		xor		rcx, rcx	; initialize rcx to 0 (null)
		test	rdi, rdi    ; test if rdi exist
		jz	_return			; if rdi does not exist go to _return

	_strlen		:
		not		rcx			; set rcx to -1
		xor		al, al		; initialize al register to 0 (null)
		cld					; clear the direction flag
		repne	scasb		; (REPeat while Not Equal) get the string length (dec rcx through NUL) 
		not		rcx			; rev all bits of negative result in absolute value
		dec		rcx			; decrement to skip the null-terminator
		jmp		_return		; go to _return
		
	_return		:
		mov		rax, rcx	; set rax to rcx result
        leave               ; releases the stack frame 
		ret					; return rax
