# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_atoi.s                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:21:39 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:21:41 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ATOI                                                        ;
; PROTOTYPE     int     ft_atoi(const char *str);                              ;
; DESIGNATION   the ft_atoi() function converts the initial portion of the     ;
;               string pointed to by str to int representation.                ;
; RETURN        the ft_atoi() function return an integer                       ;
; ============================================================================ ;

section .text
    global  _ft_atoi

    _ft_atoi        :
        push    rbp                 ; save rpb state
        mov     rbp, rsp            ; set the base pointer as it was the top of the stack
        xor     rax, rax            ; initialize rax to 0
        test    rdi, rdi            ; test if rdi is null
        jz      _return             ; if null go to _return
        push    rdi                 ; save first pointer of rdi on the stack
        mov     rsi, 1              ; set rsi to 1
        mov     rcx, 10             ; set rcx to 10
        cmp     [rdi], byte 0x2d    ; compare byte rdi if equal to '-'
        je      _negative           ; if equal go to _negative
    
    _get_number    :
        cmp     [rdi], byte 0x0     ; compare if byte of rdi is equal to '/0'
        je      _return             ; if equal then go to _return
        movzx   rbx, byte [rdi]     ; set rbx with byte of rdi
        sub     rbx, 48             ; substract 48 ('0' char - see man ascii) to rbx
        cmp     rbx, 0x0            ; compare if rbx is equal to 0
        jl      _return             ; if smaller than 0 then go to _return
        cmp     rbx, 0x9            ; compare if rbx is equal to 9
        jg      _return             ; if greater than 9 then go to _return
        mul     rcx                 ; multiply rax with rcx
        add     rax, rbx            ; add rbx to rax
        inc     rdi                 ; increment rdi
        jmp     _get_number         ; go to _get_number

    _negative       :
        mov     rsi, -1             ; set rsi to -1
        inc     rdi                 ; inc rdi
        jmp     _get_number         ; then go to _get_number

    _return         :
        imul    rsi                 ; multiply result of atoi with rsi
        leave                       ; releases the stack frame 
        ret                         ; return rax
