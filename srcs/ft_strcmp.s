# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strcmp.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:22:09 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:22:10 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_STRDUP						                               ;
; PROTOTYPE     char	*ft_strdup(const char *s1)                      	   ;
; DESIGNATION   the ft_strdup() function return a pointer to a new string,      ;
;               which is a duplicate of the string pointed to by s1. The       ;
;               returned pointer can be passed to free(). A null pointer is    ;
;               returned if the new string cannot be created                   ;
; RETURN	    the ft_strdup() function return a pointer to a new string on    ;
;               success. Otherwise, it shall return a null pointer             ;
; ============================================================================ ;

extern  _ft_strlen

section .text
    global  _ft_strcmp

    _ft_strcmp   :
        push    rbp             ; save rpb state
        mov     rbp, rsp        ; set the base pointer as it was the top of the stack
        push    rdi             ; save 1st argument on the stack
        push    rsi             ; save 2nd argument on the stack
        xor     rcx, rcx        ; initialize rcx (counter) to 0
        call    _ft_strlen      ; call ft_strlen
        mov     r9, rax         ; set r9 with the result of ft_strlen (rax)
        mov     rdi, rsi        ; set rdi with rdi value
        call    _ft_strlen      ; call ft_strlen
        cmp     r9, rax         ; compare value of r9 with result of ft_strlen (rax)
        jge     _getlen_str1    ; if r9 is greater or equal go to _getlen_str1
        jmp     _getlen_str2    ; else go to _getlen_str2
    
    _getlen_str1    :
        mov     rcx, r9         ; set rcx (counter) with r9 value (length 1st str)
        xor     r9, r9          ; initialize r9 to 0
        jmp     _strcmp         ; go to _strcmp

    _getlen_str2    :
        mov     rcx, rax        ; set rcx (counter) with rax value (length 2nd str)
        xor     rax, rax        ; intialize rax to 0
        jmp     _strcmp         ; go to _strcmp

    _strcmp         :
        cld                     ; set DF to 0
        pop     rsi             ; get rsi state (1st argument) in rsi
        pop     rdi             ; get rdi state (2nd argument) in rdi
        rep     cmpsb           ; repeat compare the byte of rsi and rdi 
        jne     _diff           ; if strings are equal go to _default
	    xor     rax, rax        ; initialize result (rax) to 0
        jmp     _return         ; then go to _return
        
	_diff		:
	    dec     rsi             ; decrement pointer 2nd string to 1 char (see cmpsb)
	    dec     rdi             ; decrement pointer 1st string to 1 char (see cmpsb)
	    mov     al, byte [rdi]  ; set al register with value of the byte of rdi
	    sub     al, byte [rsi]  ; substract value of the byte of rsi to al
	    cmp     al, 0x0         ; check the difference between al and 0
	    jl      _negative       ; if smaller than 0 then go to _negative
	    jmp     _positive       ; else go to _return
	
	_positive		:
		mov		rax, 1	        ; if 1st args is greater, then set rax to 1
		jmp		_return	        ; then go to _return
	
	_negative       :
	    mov     rax, -1         ; if 2nd args is greater, then set rax to -1
		jmp		_return         ; then go to _return
	    
	_return		:
        leave                   ; releases the stack frame 
		ret					    ; return rax
