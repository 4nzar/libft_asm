# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isalnum.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/10/14 18:11:35 by sessaidi          #+#    #+#              #
#    Updated: 2015/10/14 18:11:36 by sessaidi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

; ============================================================================ ;
; FUNCTION      FT_ISALNUM						                               ;
; PROTOTYPE     int	ft_isalnum(int c);                          	           ;
; DESIGNATION   the ft_isalnum() function tests for any character if the       ;
;               character is a digit or is a letter                            ;
;     In the ASCII character set, this includes the following characters       ;
;     (with their numeric values shown	in octal):                             ;
;                                                                              ;
;     060 ''0''    061 ''1''     062 ''2''     063 ``3''     064 ``4''         ;
;     065 ''5''    066 ''6''     067 ''7''     070 ``8''     071 ``9''         ;
;     101 ''A''    102 ''B''     103 ''C''     104 ``D''     105 ``E''         ;
;     106 ''F''    107 ''G''     110 ''H''	   111 ``I''	 112 ``J''         ;
;     113 ''K''    114 ''L''     115 ''M''	   116 ``N''	 117 ``O''         ;
;     120 ''P''    121 ''Q''     122 ''R''     123 ``S''	 124 ``T''         ;
;     125 ''U''    126 ''V''     127 ''W''	   130 ``X''	 131 ``Y''         ;
;     132 ''Z''    141 ''a''     142 ''b''     143 ``c''     144 ``d''         ;
;     145 ''e''    146 ''f''     147 ''g''     150 ``h''     151 ``i''         ;
;     152 ''j''    153 ''k''     154 ''l''     155 ``m''     156 ``n''         ;
;     157 ''o''    160 ''p''     161 ''q''     162 ``r''     163 ``s''         ;
;     164 ''t''    165 ''u''	 166 ''v''     167 ``w''     170 ``x''         ;
;     171 ''y''    172 ''z''                                                   ;
; RETURN	    the ft_isalnum() function returns zero if the character tests  ;
;               false and returns non-zero if the character tests true.        ;
; ============================================================================ ;

extern	_ft_isalpha
extern	_ft_isdigit

section	.text
	global	_ft_isalnum

	_ft_isalnum	:
        push    rbp         ; save rpb state
        mov     rbp, rsp    ; set the base pointer as it was the top of the stack
        sub     rbp, 0      ; no local variables, most compilers will omit this line
		call	_ft_isalpha	; call ft_isaplha
		cmp		rax, 0x0	; compare result of ft_isalpha if rax == 0
		jne		_return		; if result is not equal to 0 then go to _true
		call	_ft_isdigit	; else call ft_isdigit
		cmp		rax, 0x0	; compare result of ft_isdigit if rax == 0
		jne		_return		; if result is not equal to 0 then go to _true

	_return		:
        leave               ; releases the stack frame 
		ret					; return rax
